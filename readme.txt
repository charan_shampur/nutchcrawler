CSCI 572 Fall 2015 Assignment 

2) Mime types: (getImageMimeType.py) 
	Command - $ python getImageMimeType.py <Path to crawldb dataset of nutch> <mimType output file>
	Description - Identifies only image mime types
	Output - Different image types with each type in a single line, followed by the overall stats of the mime types.
	File - mimeType.txt

6)a) Near Duplicates: (near_duplicate.py)
	Command - $ python near_duplicate.py <path to segment directory> <Similarity distance value(0 to 1)> <Similarity Output file>
	Description - Identifies near duplicates
	Output - Cluster of urls grouped together 
	          [url1, duplicate 1 of url1, duplicate2 of url1.....]
	File - similarityOut.txt

6)b) Exact Duplicates: (findExactDuplicates.py)
	Command - $ python findExactDuplicates.py /path/to/segments/directory/
	Description - Identifies exact duplicates
	Output - Exactly similar urls are grouped together with each group of urls is seperated by a blank line to distinguish between clusters, output is printed on the console.
			Sample output:
			The below grouped urls have exact same images
			url1
			url2 <is a exact duplicate of url1>
			---------------------------------------------
			url3
			url4
			url5
			---------------------------------------------
			.
			.
			.
8) Nutch_Python: 
	steps : 
		1) Go to ~/nutch/runtime/local, here type bin/nutch startserver to start the local server.
		2. Open another terminal and type python RESTcrawl.py. Make sure the python has Nutch-python and nutchpy already plugged in.

2)d) File : Q2d_failed_urls.txt 
	 100 urls which were not fetched after the first crawl.

2)e) File : statistics_consolidated_q2e.txt
	 Consolidated report of urls parsed after each iteration

5)a) File : Q5a_failed_urls.txt
	 100 urls which were not fetched after tika and selenium integrated crawl.

5)d) File : statistics_consolidated_q5d.txt
	 Consolidated report of urls parsed after each iteration (crawl with enhanced tika and selenium plugin)




