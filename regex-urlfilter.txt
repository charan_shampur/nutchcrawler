# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# The default url filter.
# Better for whole-internet crawling.

# Each non-comment, non-blank line contains a regular expression
# prefixed by '+' or '-'.  The first matching pattern in the file
# determines whether a URL is included or ignored.  If no pattern
# matches, the URL is ignored.

# skip file: ftp: and mailto: urls
-^(file|ftp|mailto):

# skip image and other suffixes we can't yet parse
# for a more extensive coverage use the urlfilter-suffix plugin
-\.(css|CSS|sit|SIT|eps|EPS|wmf|WMF|zip|ZIP|ppt|PPT|mpg|MPG|xls|XLS|gz|GZ|rpm|RPM|tgz|TGZ|mov|MOV|exe|EXE|js|JS)$

# skip URLs containing certain characters as probable queries, etc.
#[?*!@=]

# skip URLs with slash-delimited segment that repeats 3+ times, to break loops
-.*(/[^/]+)/[^/]+\1/[^/]+\1/

# accept anything else
+^http://([a-z0-9A-Z]*\.)*4chan.org/k/*
+^http://([a-z0-9A-Z]*\.)*academy.com/*
+^http://([a-z0-9A-Z]*\.)*accurateshooter.com/*
+^http://([a-z0-9A-Z]*\.)*advanced-armanent.com/*
+^http://([a-z0-9A-Z]*\.)*americanlisted.com/*
+^http://([a-z0-9A-Z]*\.)*arguntrader.com/*
+^http://([a-z0-9A-Z]*\.)*armslist.com/*
+^http://([a-z0-9A-Z]*\.)*backpage.com/*
+^http://([a-z0-9A-Z]*\.)*budsgunshop.com/*
+^http://([a-z0-9A-Z]*\.)*buyusedguns.net/*
+^http://([a-z0-9A-Z]*\.)*buyusedguns.net/*
+^http://([a-z0-9A-Z]*\.)*cabelas.com/*
+^http://([a-z0-9A-Z]*\.)*cheaperthandirt.com/*
+^http://([a-z0-9A-Z]*\.)*davidsonsinc.com/*
+^http://([a-z0-9A-Z]*\.)*firearmlist.com/*
+^http://([a-z0-9A-Z]*\.)*firearmslist.com/*
+^http://([a-z0-9A-Z]*\.)*freeclassifieds.com/*
+^http://([a-z0-9A-Z]*\.)*freegunclassifieds.com/*
+^http://([a-z0-9A-Z]*\.)*freegunclaXssifieds.com/*
+^http://([a-z0-9A-Z]*\.)*gandermountain.com/*
+^http://([a-z0-9A-Z]*\.)*gunauction.com/*
+^http://([a-z0-9A-Z]*\.)*gunbroker.com/*
+^http://([a-z0-9A-Z]*\.)*gunbroker.com/*
+^http://([a-z0-9A-Z]*\.)*gundeals.org/*
+^http://([a-z0-9A-Z]*\.)*gunlistings.org/*
+^http://([a-z0-9A-Z]*\.)*gunlistings.org/*
+^http://([a-z0-9A-Z]*\.)*gunsamerica.com/*
+^http://([a-z0-9A-Z]*\.)*gunsinternational.com/*
+^http://([a-z0-9A-Z]*\.)*guntrader.com/*
+^http://([a-z0-9A-Z]*\.)*hipointfirearmsforums.com/*
+^http://([a-z0-9A-Z]*\.)*impactguns.com/*
+^http://([a-z0-9A-Z]*\.)*iwanna.com/*
+^http://([a-z0-9A-Z]*\.)*lionseek.com/*
+^http://([a-z0-9A-Z]*\.)*midwestguntrader.com/*
+^http://([a-z0-9A-Z]*\.)*nationalguntrader.com/*
+^http://([a-z0-9A-Z]*\.)*nationalguntrader.com/*
+^http://([a-z0-9A-Z]*\.)*nextechclassifieds.com/categories/sporting-goods/firearms/*
+^http://([a-z0-9A-Z]*\.)*oodle.com/*
+^http://([a-z0-9A-Z]*\.)*recycler.com/*
+^http://([a-z0-9A-Z]*\.)*shooterswap.com/*
+^http://([a-z0-9A-Z]*\.)*shooting.org/*
+^http://([a-z0-9A-Z]*\.)*slickguns.com/*
+^http://([a-z0-9A-Z]*\.)*wantaddigest.com/*
+^http://([a-z0-9A-Z]*\.)*wikiarms.com/guns/*
+^http://([a-z0-9A-Z]*\.)*abqjournal.com/*
+^http://([a-z0-9A-Z]*\.)*alaskaslist.com/*
+^http://([a-z0-9A-Z]*\.)*billingsthriftynickel.com/*
+^http://([a-z0-9A-Z]*\.)*carolinabargaintrader.net/*
+^http://([a-z0-9A-Z]*\.)*carolinabargaintrader.net/*
+^http://([a-z0-9A-Z]*\.)*clasificadosphoenix.univision.com/*
+^http://([a-z0-9A-Z]*\.)*classifiednc.com/*
+^http://([a-z0-9A-Z]*\.)*classifieds.al.com/*
+^http://([a-z0-9A-Z]*\.)*cologunmarket.com/*
+^http://([a-z0-9A-Z]*\.)*comprayventadearms.com/*
+^http://([a-z0-9A-Z]*\.)*dallasguns.com/*
+^http://([a-z0-9A-Z]*\.)*elpasoguntrader.com/*
+^http://([a-z0-9A-Z]*\.)*fhclassifieds.com/*
+^http://([a-z0-9A-Z]*\.)*floridagunclassifieds.com/*
+^http://([a-z0-9A-Z]*\.)*floridaguntrader.com/*
+^http://([a-z0-9A-Z]*\.)*gowilkes.com/*
+^http://([a-z0-9A-Z]*\.)*gunidaho.com/*
+^http://([a-z0-9A-Z]*\.)*hawaiiguntrader.com/*
+^http://([a-z0-9A-Z]*\.)*iguntrade.com/*
+^http://([a-z0-9A-Z]*\.)*jasonsguns.com/*
+^http://([a-z0-9A-Z]*\.)*ksl.com/*
+^http://([a-z0-9A-Z]*\.)*kyclassifieds.com/*
+^http://([a-z0-9A-Z]*\.)*midutahradio.com/tradio/*
+^http://([a-z0-9A-Z]*\.)*midwestgtrader.com/*
+^http://([a-z0-9A-Z]*\.)*montanagunclassifieds.com/*
+^http://([a-z0-9A-Z]*\.)*mountaintrader.com/*
+^http://([a-z0-9A-Z]*\.)*msguntrader.com/*
+^http://([a-z0-9A-Z]*\.)*ncgunads.com/*
+^http://([a-z0-9A-Z]*\.)*newmexicoguntrader.com/*
+^http://([a-z0-9A-Z]*\.)*nextechclassifieds.com/*
+^http://([a-z0-9A-Z]*\.)*sanjoseguntrader.com/*
+^http://([a-z0-9A-Z]*\.)*tell-n-sell.com/*
+^http://([a-z0-9A-Z]*\.)*tennesseegunexchange.com/*
+^http://([a-z0-9A-Z]*\.)*theoutdoorstrader.com/*
+^http://([a-z0-9A-Z]*\.)*tradesnsales.com/*
+^http://([a-z0-9A-Z]*\.)*upstateguntrader.com/*
+^http://([a-z0-9A-Z]*\.)*vci-classifieds.com/*
+^http://([a-z0-9A-Z]*\.)*zidaho.com/*
+^http://([a-z0-9A-Z]*\.)*texasguntrader.com/*