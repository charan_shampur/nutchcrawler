__author__ = 'sharan'

from nutch.nutch import Nutch
import nutchpy
from nutch.nutch import Server
from nutch.nutch import Seed
from nutch.nutch import SeedClient

#list of urls
lisst = ['http://www.4chan.org/k/','http://www.academy.com/shop/browse/hunting','http://www.accurateshooter.com/','http://www.advanced-armanent.com/','http://www.arguntrader.com/','http://www.armslist.com/','http://www.budsgunshop.com/','http://www.buyusedguns.net/','http://www.cheaperthandirt.com/']
#number of rounds
n_rounds = 10
#Nutch instance
nt = Nutch()
#server instance
val = Server('http://localhost:8081')
#Seedclient instance
seedcl=SeedClient(val)
#Seed instance
seedval = seedcl.create('Seed_guns',lisst)
#crawl method
crawlclient =nt.Crawl(seedval)
#increasing the rounds
crawlclient.addRounds(n_rounds)
#wating for all jobs to finish
llst =crawlclient.waitAll()
#this prints the list of job handlers for each round as a list.
print(llst)
